# installer_for_machine_learning

The above shell script provides all required packages for Machine Learning in Python. Not just Machine Learning, the scripts includes packages for natural language processing, Recommender System and Speech Processing. You can use this file in any debian based Operating Systems such as Ubuntu, Mint etc

The downloaded script is to be given permission using 'chmod 777 installer_for_machine_learning.sh' and the execute the file './installer_for_machine_learning.sh' with admin permissions. 

The shell script file provides the most commonly used python editor (Jupyter Notebook) for machine learning. The file will be updated with new machine learning packages whenever needed.

Happy Learning.


