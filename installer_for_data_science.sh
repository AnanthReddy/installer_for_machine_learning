#!/bin/bash

apt-get update
apt-get upgrade

# Installing Python3.6 and pip
apt-get install python3
apt-get install python3-pip

# Basic Data Science modules
pip3 install pandas
pip3 install xlrd
pip3 install sklearn
pip3 install xgboost
pip3 install lightgbm
pip3 install swifter
pip3 install pandas_profiling
pip3 install pivottablejs
pip3 install scipy
pip3 install matplotlib
pip3 install seaborn
pip3 install pyemd
pip3 install scipy
pip3 install geomstats
pip3 install torch
pip3 install boto3
pip3 install regex
pip3 install virtualenv
pip3 install google-cloud-storage

# Installing Jupyter notebook (a preferred editor)
pip3 install jupyter

# Natural Language Processing Modules
pip3 install nltk
pip3 install Cython
pip3 install spacy
pip3 install autocorrect
pip3 install fuzzy
pip3 install fasttext
pip3 install tensorflow
pip3 install keras

# Speech Recognition Modules
pip3 install SpeechRecognition
pip3 install pyttsx3
pip3 install wit

# Recommender System
pip3 install lightfm
pip3 install elasticsearch

# Others
pip3 install knockknock
pip3 install sis
pip3 install ipython-autotime

apt-get update
apt-get upgrade
